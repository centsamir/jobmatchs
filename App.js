import { StyleSheet, Text, View } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

const tab = createBottomTabNavigator();

function homeScreen() {
  return (
    <View style={styles.container}>
      <Text>Home</Text>
    </View>
  );
}

function messageScreen() {
  return (
    <View style={styles.container}>
      <Text>Messages</Text>
    </View>
  );
}

function profileScreen() {
  return (
    <View style={styles.container}>
      <Text>Mon profil</Text>
    </View>
  );
}
export default function App() {
  return (
    <NavigationContainer>
      <tab.Navigator>
        <tab.Screen name="Accueil" component={homeScreen} />
        <tab.Screen name="Messages" component={messageScreen} />
        <tab.Screen name="Mon profil" component={profileScreen} />
      </tab.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
